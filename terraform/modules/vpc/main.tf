resource "google_compute_firewall" "firewall_ssh" {
  name        = "default-allow-ssh-${var.inst_suff}"
  network     = "default"
  description = "Allow ssh from any"
  priority    = "65534"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = "${var.source_ranges}"
}

resource "google_compute_firewall" "firewall_thesaurus" {
  name    = "allow-thesaurus-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"

    ports = ["8585"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["thesaurus-app"]
}

resource "google_compute_firewall" "firewall_proxy_app" {
  name    = "allow-proxy-app-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"

    ports = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["thesaurus-app"]
}

resource "google_compute_firewall" "firewall_mongodb" {
  name    = "allow-mongodb-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["27017"]
  }

  source_tags = ["thesaurus-app"]
  target_tags = ["thesaurus-db"]
}

resource "google_compute_firewall" "firewall_mongodb-exporter" {
  name    = "allow-mongodb-exporter-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["9104"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["thesaurus-app"]
}

resource "google_compute_firewall" "firewall_cadvisor" {
  name    = "allow-cadvisor-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["8080"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["thesaurus-app", "thesaurus-mon"]
}

resource "google_compute_firewall" "firewall_node_exporter" {
  name    = "allow-node-exporter-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["9100"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["thesaurus-app", "thesaurus-mon"]
}

resource "google_compute_firewall" "firewall_prometheus" {
  name    = "allow-prometheus-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["9090"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["thesaurus-mon"]
}

resource "google_compute_firewall" "firewall_grafana" {
  name    = "allow-grafana-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["3000"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["thesaurus-mon"]
}

resource "google_compute_firewall" "firewall_alertmanager" {
  name    = "allow-alertmanager-default-${var.inst_suff}"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["9093"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["thesaurus-mon"]
}
