resource "google_compute_instance" "app" {
  name         = "thesaurus-app-${var.inst_suff}"
  machine_type = "g1-small"
  zone         = "${var.zone}"
  tags         = ["thesaurus-app"]

  boot_disk {
    initialize_params {
      image = "${var.app_disk_image}"
    }
  }

  network_interface {
    network = "default"

    access_config = {
      #      nat_ip = "${google_compute_address.app_ip.address}"
    }
  }

  metadata {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
}

resource "null_resource" "app-runtime" {
  triggers = {
    app-runtime_instance_ids = "google_compute_instance.app.id"
  }

  connection {
    host = "${google_compute_instance.app.network_interface.0.access_config.0.nat_ip}"
  }

  provisioner "file" {
    source      = "files/thesaurus"
    destination = "/opt/containers"

    connection {
      type    = "ssh"
      user    = "appuser"
      timeout = "1m"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd /opt/containers/thesaurus",
      "sudo docker-compose up -d",
      "sleep 10",
      "sudo docker exec thesaurus_server thesaurus --update --csv.path /opt/dict",
      "sudo systemctl restart docker",
    ]

    connection {
      type    = "ssh"
      user    = "appuser"
      timeout = "1m"
    }
  }

  provisioner "file" {
    source      = "files/cadvisor"
    destination = "/opt/containers"

    connection {
      type    = "ssh"
      user    = "appuser"
      timeout = "1m"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd /opt/containers/cadvisor",
      "sudo docker-compose up -d",
      "sudo systemctl restart docker",
    ]

    connection {
      type    = "ssh"
      user    = "appuser"
      timeout = "1m"
    }
  }

  provisioner "file" {
    source      = "files/node-exporter"
    destination = "/opt/containers"

    connection {
      type    = "ssh"
      user    = "appuser"
      timeout = "1m"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd /opt/containers/node-exporter",
      "sudo docker-compose up -d",
      "sudo systemctl restart docker",
    ]

    connection {
      type    = "ssh"
      user    = "appuser"
      timeout = "1m"
    }
  }
}
