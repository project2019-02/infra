resource "google_compute_instance" "mon" {
  name         = "thesaurus-mon-${var.inst_suff}"
  machine_type = "g1-small"
  zone         = "${var.zone}"
  tags         = ["thesaurus-mon"]

  boot_disk {
    initialize_params {
      image = "${var.mon_disk_image}"
    }
  }

  network_interface {
    network = "default"

    access_config = {
      #      nat_ip = "${google_compute_address.mon_ip.address}"
    }
  }

  metadata {
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
}

resource "null_resource" "monitor-runtime" {
  triggers = {
    monitor_instance_ids = "google_compute_instance.mon.id"
  }

  connection {
    host = "${google_compute_instance.mon.network_interface.0.access_config.0.nat_ip}"
  }

  provisioner "file" {
    source      = "files/monitor"
    destination = "/opt/containers"

    connection {
      type    = "ssh"
      user    = "appuser"
      timeout = "1m"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd /opt/containers/monitor",
      "sudo docker-compose up -d",
      "sudo systemctl restart docker",
    ]

    connection {
      type    = "ssh"
      user    = "appuser"
      timeout = "1m"
    }
  }
}
