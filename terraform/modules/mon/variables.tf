variable public_key_path {
  description = "Path to the public key used to connect to instance"
}

variable zone {
  description = "Zone"
}

variable mon_disk_image {
  description = "Disk image for monitor"
  default     = "common-base"
}

variable inst_suff {
  description = "Suffix for instances"
  default     = "undef"
}
