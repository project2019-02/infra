variable project {
  description = "Project ID"
}

variable region {
  description = "Region"

  # Значение по умолчанию
  default = "europe-west1"
}

variable zone {
  description = "Zone"

  # Значение по умолчанию
  default = "europe-west1-d"
}

variable public_key_path {
  # Описание переменной
  description = "Path to the public key used for ssh access"
}

variable private_key_path {
  # Описание переменной
  description = "Path to the private key used for ssh access"
}

variable disk_image {
  description = "Disk image"
}

variable app_disk_image {
  description = "Disk image for thesaurus app"
  default     = "common-base"
}

variable mon_disk_image {
  description = "Disk image for thesaurus monitor"
  default     = "common-base"
}

variable inst_suff {
  description = "Suffix for instances"
  default     = "undef"
}
