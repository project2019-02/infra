output "app_external_ip" {
  value = "${module.app.app_external_ip}"
}

output "mon_external_ip" {
  value = "${module.mon.mon_external_ip}"
}
