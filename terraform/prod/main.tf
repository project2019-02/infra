terraform {
  # Версия terraform
  required_version = ">=0.11.11,<0.12"
}

provider "google" {
  # Версия провайдера
  version = "2.5.0"

  # ID проекта
  project = "${var.project}"

  region = "${var.region}"
}

provider "null" {}

module "app" {
  source          = "../modules/app"
  public_key_path = "${var.public_key_path}"
  zone            = "${var.zone}"
  app_disk_image  = "${var.app_disk_image}"

  inst_suff = "prod"
}

module "mon" {
  source          = "../modules/mon"
  public_key_path = "${var.public_key_path}"
  zone            = "${var.zone}"
  mon_disk_image  = "${var.mon_disk_image}"

  inst_suff = "prod"
}

module "vpc" {
  source        = "../modules/vpc"
  source_ranges = ["0.0.0.0/0"]

  inst_suff = "prod"
}
