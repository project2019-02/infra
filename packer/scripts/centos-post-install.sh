#!/bin/bash

sed -i -e s/"SELINUX=enforcing"/"SELINUX=disabled"/g /etc/selinux/config
yum update -y
yum install -y epel-release 

yum install -y mc screen tcpdump man wget ethtool net-tools \
  htop iotop iftop atop nc bzip2 zip unzip git jq

CONF_FILE=/etc/profile.d/bash_hist.sh
cat << EOF > ${CONF_FILE}
#!/bin/bash

HISTSIZE=99999;
export HISTSIZE;

HISTCONTROL="ignoreboth";
export HISTCONTROL;
EOF
chmod u+x ${CONF_FILE}

cd /etc
[ -z `egrep "net.ipv6.conf.all.disable_ipv6" sysctl.conf` ] && echo -e 'net.ipv6.conf.all.disable_ipv6 = 1' >> sysctl.conf
[ -z `egrep "net.ipv6.conf.default.disable_ipv6" sysctl.conf` ] && echo -e 'net.ipv6.conf.default.disable_ipv6 = 1' >> sysctl.conf
sysctl -p

cd /etc/sysconfig
[ -z `egrep NOZEROCONF network` ] && echo -e '\nNOZEROCONF=yes' >> network

sed -i -e s/"vbell on"/"vbell off"/g /etc/screenrc
sed -i -e s/"defscrollback 1000"/"defscrollback 99999"/g /etc/screenrc
sed -i -e s/"#termcap xterm 'is\=\\\E\[r\\\E\[m\\\E\[2J\\\E\[H\\\E\[?7h\\\E\[?1\;4\;6l'"/"termcap xterm 'is\=\\\E\[r\\\E\[m\\\E\[2J\\\E\[H\\\E\[?7h\\\E\[?1\;4;6l'"/g /etc/screenrc
sed -i -e s/"#terminfo xterm 'is\=\\\E\[r\\\E\[m\\\E\[2J\\\E\[H\\\E\[?7h\\\E\[?1\;4\;6l'"/"terminfo xterm 'is\=\\\E\[r\\\E\[m\\\E\[2J\\\E\[H\\\E\[?7h\\\E\[?1\;4;6l'"/g /etc/screenrc

### Docker infrastructure - BEGIN
#

yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io

curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/sbin/docker-compose
chmod +x /usr/sbin/docker-compose
mkdir -p /opt/containers
chmod o+w /opt/containers
echo PATH=$PATH

systemctl daemon-reload
systemctl enable docker
systemctl start docker
systemctl status docker

docker version
docker-compose version

#
### Docker infrastructure - END

systemctl daemon-reload
systemctl disable firewalld
systemctl stop firewalld
systemctl disable NetworkManager
systemctl mask NetworkManager
systemctl stop NetworkManager
systemctl disable NetworkManager-dispatcher.service
systemctl stop NetworkManager-dispatcher.service
systemctl disable NetworkManager-wait-online.service
systemctl stop NetworkManager-wait-online.service
